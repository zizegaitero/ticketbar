/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.Empresa;
import com.zizegaitero.ticketbar.entidades.Empresa_;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class EmpresaRepository {

    /**
     * lista todos os tipos de produtos
     *
     * @return
     */
    public List<Empresa> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Empresa> from = query.from(Empresa.class);
        query.select(from);
        List<Empresa> tipoProdutos = em.createQuery(query).getResultList();
        return tipoProdutos;
    }

    /**
     * salva um tipo de produto
     *
     * @param empresa
     * @return
     * @throws Exception
     */
    public Empresa salvaEmpresa(Empresa empresa) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        empresa.setDthrcadastro(new Date());
        em.merge(empresa);
        em.getTransaction().commit();
        return empresa;
    }

    /**
     * Busca o próximo código
     * @return
     * @throws Exception 
     */
    public Long buscaProximoSequencial() throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Empresa> from = query.from(Empresa.class);
        query.select(from);
        query.orderBy(cb.desc(from.get(Empresa_.codigo)));
        try {
            Empresa tipoProduto = (Empresa) em.createQuery(query).
                    setMaxResults(1).
                    getSingleResult();
            return tipoProduto.getCodigo() + 1;
        } catch (NoResultException ex) {
            return 1L;
        }

    }
    
    /**
     * retorna todas as empresas ativas
     * @return 
     */
    public List<Empresa> findOnlyAtivo() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Empresa> from = query.from(Empresa.class);
        query.select(from);
        query.where(cb.equal(from.get(Empresa_.idativo), Boolean.TRUE));
        List<Empresa> empresas = em.createQuery(query).getResultList();
        return empresas;
    }

}
