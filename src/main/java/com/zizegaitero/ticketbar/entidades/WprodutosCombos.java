/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "WPRODUTOS_COMBOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WprodutosCombos.findAll", query = "SELECT w FROM WprodutosCombos w"),
    @NamedQuery(name = "WprodutosCombos.findByCodigo", query = "SELECT w FROM WprodutosCombos w WHERE w.codigo = :codigo"),
    @NamedQuery(name = "WprodutosCombos.findByIdprodutoinfo", query = "SELECT w FROM WprodutosCombos w WHERE w.idprodutoinfo = :idprodutoinfo"),
    @NamedQuery(name = "WprodutosCombos.findByIdcomboinfo", query = "SELECT w FROM WprodutosCombos w WHERE w.idcomboinfo = :idcomboinfo"),
    @NamedQuery(name = "WprodutosCombos.findByDescricao", query = "SELECT w FROM WprodutosCombos w WHERE w.descricao = :descricao"),
    @NamedQuery(name = "WprodutosCombos.findByValor", query = "SELECT w FROM WprodutosCombos w WHERE w.valor = :valor"),
    @NamedQuery(name = "WprodutosCombos.findByIdativo", query = "SELECT w FROM WprodutosCombos w WHERE w.idativo = :idativo"),
    @NamedQuery(name = "WprodutosCombos.findByTipo", query = "SELECT w FROM WprodutosCombos w WHERE w.tipo = :tipo")})
public class WprodutosCombos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "CODIGO")
    private BigInteger codigo;
    @Column(name = "IDPRODUTOINFO")
    private BigInteger idprodutoinfo;
    @Column(name = "IDCOMBOINFO")
    private BigInteger idcomboinfo;
    @Column(name = "DESCRICAO")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Column(name = "IDATIVO")
    private Boolean idativo;
    @Column(name = "TIPO")
    private String tipo;

    public WprodutosCombos() {
    }

    public BigInteger getCodigo() {
        return codigo;
    }

    public void setCodigo(BigInteger codigo) {
        this.codigo = codigo;
    }

    public BigInteger getIdprodutoinfo() {
        return idprodutoinfo;
    }

    public void setIdprodutoinfo(BigInteger idprodutoinfo) {
        this.idprodutoinfo = idprodutoinfo;
    }

    public BigInteger getIdcomboinfo() {
        return idcomboinfo;
    }

    public void setIdcomboinfo(BigInteger idcomboinfo) {
        this.idcomboinfo = idcomboinfo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Boolean getIdativo() {
        return idativo;
    }

    public void setIdativo(Boolean idativo) {
        this.idativo = idativo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.codigo);
        hash = 59 * hash + Objects.hashCode(this.idprodutoinfo);
        hash = 59 * hash + Objects.hashCode(this.idcomboinfo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WprodutosCombos other = (WprodutosCombos) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.idprodutoinfo, other.idprodutoinfo)) {
            return false;
        }
        if (!Objects.equals(this.idcomboinfo, other.idcomboinfo)) {
            return false;
        }
        return true;
    }
    
    
    
}
