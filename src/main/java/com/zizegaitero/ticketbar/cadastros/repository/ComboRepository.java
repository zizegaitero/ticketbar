/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.Combo;
import com.zizegaitero.ticketbar.entidades.ComboInfo;
import com.zizegaitero.ticketbar.entidades.ComboInfo_;
import com.zizegaitero.ticketbar.entidades.Combo_;
import com.zizegaitero.ticketbar.entidades.ProdutosInfo;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class ComboRepository {

    /**
     * lista todos os tipos de produtos
     *
     * @return
     */
    public List<ComboInfo> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ComboInfo> from = query.from(ComboInfo.class);
        query.select(from);
        query.where(cb.equal(from.get(ComboInfo_.idativo), Boolean.TRUE));
        List<ComboInfo> tipoCombo = em.createQuery(query).getResultList();
        return tipoCombo;
    }

    /**
     * salva um tipo de produto
     *
     * @param ComboInfo
     * @return
     * @throws Exception
     */
    public ComboInfo salvaCombo(ComboInfo ComboInfo) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        this.desativaInfos(ComboInfo.getIdCombo(), em);
        ComboInfo.setId(null);
        ComboInfo.setIdativo(Boolean.TRUE);
        ComboInfo.setDthrcadastro(new Date());
        em.merge(ComboInfo);
        em.getTransaction().commit();
        return ComboInfo;
    }

    /**
     *
     * @param combo
     * @param em
     */
    private void desativaInfos(Combo combo, EntityManager em) {
        if (combo.getId() != null) {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaUpdate<ComboInfo> createCriteriaUpdate = cb.createCriteriaUpdate(ComboInfo.class);
            Root e = createCriteriaUpdate.from(ComboInfo.class);
            createCriteriaUpdate.set(e.get(ComboInfo_.idativo), Boolean.FALSE);
            createCriteriaUpdate.where(cb.and(
                    cb.equal(e.get(ComboInfo_.idCombo), combo),
                    cb.equal(e.get(ComboInfo_.idativo), Boolean.TRUE)
            ));
            em.createQuery(createCriteriaUpdate).executeUpdate();
        }
    }

    /**
     * Busca somente os produtos ativos
     * @return 
     */
    public List<ComboInfo> findOnlyAtivo() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ComboInfo> from = query.from(ComboInfo.class);
        query.select(from);
        query.where(cb.and(
                cb.equal(from.get(ComboInfo_.idativo), Boolean.TRUE),
                cb.equal(from.get(ComboInfo_.idCombo).get(Combo_.idativo), Boolean.TRUE)
                ));
        List<ComboInfo> tipoCombo = em.createQuery(query).getResultList();
        return tipoCombo;
    }
    
    public ComboInfo findOneInfo(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        return em.find(ComboInfo.class, id);
    }
    

}
