/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.Produtos;
import com.zizegaitero.ticketbar.entidades.ProdutosInfo;
import com.zizegaitero.ticketbar.entidades.ProdutosInfo_;
import com.zizegaitero.ticketbar.entidades.Produtos_;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class ProdutosRepository {

    /**
     * lista todos os tipos de produtos
     *
     * @return
     */
    public List<ProdutosInfo> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ProdutosInfo> from = query.from(ProdutosInfo.class);
        query.select(from);
        query.where(cb.equal(from.get(ProdutosInfo_.idativo), Boolean.TRUE));
        List<ProdutosInfo> tipoProdutos = em.createQuery(query).getResultList();
        return tipoProdutos;
    }

    /**
     * salva um tipo de produto
     *
     * @param produtosInfo
     * @return
     * @throws Exception
     */
    public ProdutosInfo salvaProdutos(ProdutosInfo produtosInfo) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        this.desativaInfos(produtosInfo.getIdProduto(), em);
        produtosInfo.setId(null);
        produtosInfo.setIdativo(Boolean.TRUE);
        produtosInfo.setDthrcadastro(new Date());
        em.merge(produtosInfo);
        em.getTransaction().commit();
        return produtosInfo;
    }

    /**
     *
     * @param produtos
     * @param em
     */
    private void desativaInfos(Produtos produtos, EntityManager em) {
        if (produtos.getId() != null) {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaUpdate<ProdutosInfo> createCriteriaUpdate = cb.createCriteriaUpdate(ProdutosInfo.class);
            Root e = createCriteriaUpdate.from(ProdutosInfo.class);
            createCriteriaUpdate.set(e.get(ProdutosInfo_.idativo), Boolean.FALSE);
            createCriteriaUpdate.where(cb.and(
                    cb.equal(e.get(ProdutosInfo_.idProduto), produtos),
                    cb.equal(e.get(ProdutosInfo_.idativo), Boolean.TRUE)
            ));
            em.createQuery(createCriteriaUpdate).executeUpdate();
        }
    }
    
    public ProdutosInfo findOneInfo(Long id) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        return em.find(ProdutosInfo.class, id);
    }

    /**
     * Busca somente os produtos ativos
     * @return 
     */
    public List<ProdutosInfo> findOnlyAtivo() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<ProdutosInfo> from = query.from(ProdutosInfo.class);
        query.select(from);
        query.where(cb.and(
                cb.equal(from.get(ProdutosInfo_.idativo), Boolean.TRUE),
                cb.equal(from.get(ProdutosInfo_.idProduto).get(Produtos_.idativo), Boolean.TRUE)
                ));
        List<ProdutosInfo> tipoProdutos = em.createQuery(query).getResultList();
        return tipoProdutos;
    }

}
