/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.controller;

import com.zizegaitero.ticketbar.cadastros.repository.ComboRepository;
import com.zizegaitero.ticketbar.cadastros.repository.ProdutosRepository;
import com.zizegaitero.ticketbar.cadastros.repository.WProdutosCombosRepository;
import com.zizegaitero.ticketbar.entidades.ComboInfo;
import com.zizegaitero.ticketbar.entidades.ComboItem;
import com.zizegaitero.ticketbar.util.AbstractController;
import componentes.Srh.Utils.FormatUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author jean
 */
public class ComboController  extends AbstractController {
    
    private final ComboRepository repository;
    private final ProdutosRepository produtosRepository;
    private final WProdutosCombosRepository wProdutosCombosRepository;

    public ComboController() {
        this.repository = new ComboRepository();
        this.produtosRepository = new ProdutosRepository();
        this.wProdutosCombosRepository = new WProdutosCombosRepository();
    }
    
    public ComboInfo salvaCombo(ComboInfo prodi) throws Exception{
        prodi = this.repository.salvaCombo(prodi);
        return prodi;
    }
    
    public List montaTabela(Integer casas){
        List<ComboInfo> findAll = this.repository.findAll();
        List<Object> lista = new ArrayList<>();
        findAll.forEach((cont) -> {
            lista.add(criaListParaTabela(cont,
                    FormatUtil.formataNumero(casas,cont.getIdCombo().getCodigo()), 
                    cont.getIdCombo().getDescricao(),
                    cont.getValor(),
                    cont.getIdCombo().getIdativo(),
                    cont.getComboItemCollection().size(),
                    FormatUtil.formataDataHora(cont.getDthrcadastro())));
        });    
        return lista;
    }
    
    public List montaTabelaItens(Collection<ComboItem> itens){
        List<Object> lista = new ArrayList<>();
        for (ComboItem cont : itens) {
            lista.add(criaListParaTabela(cont,
                    cont.getIdProdutoInfo().getIdProduto().getDescricao()+"-"+ 
                    cont.getIdProdutoInfo().getIdProduto().getIdTipoProduto().getDescricao(),
                    cont.getQuantidade(),
                    cont.getValor()));
        }   
        return lista;
    }
    
    public Long buscaSequencial() throws Exception {
        Long prod = this.wProdutosCombosRepository.buscaProximoSequencial();
        return prod;        
    }
    
    public List montaComboProdutos() throws Exception {
        return montaComboProdutosInfo(this.produtosRepository.findAll());
    }
    
}
