/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "WVENDAS_ITENS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WvendasItens.findAll", query = "SELECT w FROM WvendasItens w"),
    @NamedQuery(name = "WvendasItens.findById", query = "SELECT w FROM WvendasItens w WHERE w.id = :id"),
    @NamedQuery(name = "WvendasItens.findByIdvenda", query = "SELECT w FROM WvendasItens w WHERE w.idvenda = :idvenda"),
    @NamedQuery(name = "WvendasItens.findByCodigoProduto", query = "SELECT w FROM WvendasItens w WHERE w.codigoProduto = :codigoProduto"),
    @NamedQuery(name = "WvendasItens.findByDescricaoProduto", query = "SELECT w FROM WvendasItens w WHERE w.descricaoProduto = :descricaoProduto"),
    @NamedQuery(name = "WvendasItens.findByQuantidade", query = "SELECT w FROM WvendasItens w WHERE w.quantidade = :quantidade"),
    @NamedQuery(name = "WvendasItens.findByValor", query = "SELECT w FROM WvendasItens w WHERE w.valor = :valor"),
    @NamedQuery(name = "WvendasItens.findByValorTotal", query = "SELECT w FROM WvendasItens w WHERE w.valorTotal = :valorTotal"),
    @NamedQuery(name = "WvendasItens.findByCodigoVenda", query = "SELECT w FROM WvendasItens w WHERE w.codigoVenda = :codigoVenda"),
    @NamedQuery(name = "WvendasItens.findByDthrcadastro", query = "SELECT w FROM WvendasItens w WHERE w.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "WvendasItens.findByValorVenda", query = "SELECT w FROM WvendasItens w WHERE w.valorVenda = :valorVenda"),
    @NamedQuery(name = "WvendasItens.findByDescricao", query = "SELECT w FROM WvendasItens w WHERE w.descricao = :descricao"),
    @NamedQuery(name = "WvendasItens.findByDtevento", query = "SELECT w FROM WvendasItens w WHERE w.dtevento = :dtevento"),
    @NamedQuery(name = "WvendasItens.findByIdevento", query = "SELECT w FROM WvendasItens w WHERE w.idevento = :idevento")})
public class WvendasItens implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "ID")
    @Id
    private BigInteger id;
    @Column(name = "IDVENDA")
    private BigInteger idvenda;
    @Column(name = "CODIGO_PRODUTO")
    private BigInteger codigoProduto;
    @Column(name = "DESCRICAO_PRODUTO")
    private String descricaoProduto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "QUANTIDADE")
    private BigDecimal quantidade;
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Column(name = "VALOR_TOTAL")
    private BigDecimal valorTotal;
    @Column(name = "CODIGO_VENDA")
    private BigInteger codigoVenda;
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @Column(name = "VALOR_VENDA")
    private BigDecimal valorVenda;
    @Column(name = "DESCRICAO")
    private String descricao;
    @Column(name = "DTEVENTO")
    @Temporal(TemporalType.DATE)
    private Date dtevento;
    @Column(name = "IDEVENTO")
    private BigInteger idevento;

    public WvendasItens() {
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getIdvenda() {
        return idvenda;
    }

    public void setIdvenda(BigInteger idvenda) {
        this.idvenda = idvenda;
    }

    public BigInteger getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(BigInteger codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigInteger getCodigoVenda() {
        return codigoVenda;
    }

    public void setCodigoVenda(BigInteger codigoVenda) {
        this.codigoVenda = codigoVenda;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public BigDecimal getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtevento() {
        return dtevento;
    }

    public void setDtevento(Date dtevento) {
        this.dtevento = dtevento;
    }

    public BigInteger getIdevento() {
        return idevento;
    }

    public void setIdevento(BigInteger idevento) {
        this.idevento = idevento;
    }
    
}
