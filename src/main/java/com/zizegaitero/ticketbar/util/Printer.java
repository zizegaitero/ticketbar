/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.util;

import com.zizegaitero.ticketbar.entidades.WvendasItens;
import com.zizegaitero.ticketbar.vendas.controller.VendasItemRelatorioObject;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author jean
 */
public class Printer {

    public void imprimeTicket(List<VendasItemRelatorioObject> lst) {
        JRBeanCollectionDataSource ds;
        InputStream in;
        JasperReport jasper;
        Map params;
        JasperPrint jasperPrint;
        byte[] logo;

        try {
            ds = new JRBeanCollectionDataSource(lst);
            //Trata o relatorio e gera o mesmo.
            in = getClass().getResourceAsStream("/com/zizegaitero/relatorios/ticket.jasper");
            jasper = (JasperReport) JRLoader.loadObject(in);
            //Data do relatorio.
            params = new HashMap();
            if (lst.size() > 0) {
                logo = lst.get(0).getLogotipo();
                ImageIcon imageIcon = new ImageIcon(logo);
                params.put("logo_empresa", imageIcon.getImage());
            }
            //params.put("IS_IGNORE_PAGINATION", true);

            jasperPrint = JasperFillManager.fillReport(jasper, params, ds);
//            JRPdfExporter exporter = new JRPdfExporter();
//            File file = new File("/home/jean/ticket.pdf");
//            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
//
//            exporter.exportReport();
            JasperPrintManager.printReport(jasperPrint, false);
            //JasperViewer jv = new JasperViewer(jasperPrint);
            //jv.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void imprimeRelatorioEvento(List<WvendasItens> lst, byte[] logo, String nomeEmpresa) {
        JRBeanCollectionDataSource ds;
        InputStream in;
        JasperReport jasper;
        Map params;
        JasperPrint jasperPrint;
        try {
            ds = new JRBeanCollectionDataSource(lst);
            //Trata o relatorio e gera o mesmo.
            in = getClass().getResourceAsStream("/com/zizegaitero/relatorios/Resumo_venda_simplificado.jasper");
            jasper = (JasperReport) JRLoader.loadObject(in);
            //Data do relatorio.
            params = new HashMap();
            ImageIcon imageIcon = new ImageIcon(logo);
            params.put("logo_empresa", imageIcon.getImage());
            params.put("Empresa", nomeEmpresa);
            //params.put("IS_IGNORE_PAGINATION", true);

            jasperPrint = JasperFillManager.fillReport(jasper, params, ds);
//            JRPdfExporter exporter = new JRPdfExporter();
//            File file = new File("/home/jean/ticket.pdf");
//            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(file));
//
//            exporter.exportReport();
            //JasperPrintManager.printReport(jasperPrint, true);
            JasperViewer jv = new JasperViewer(jasperPrint, false);
            jv.setVisible(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
