/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "EVENTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventos.findAll", query = "SELECT e FROM Eventos e"),
    @NamedQuery(name = "Eventos.findById", query = "SELECT e FROM Eventos e WHERE e.id = :id"),
    @NamedQuery(name = "Eventos.findByCodigo", query = "SELECT e FROM Eventos e WHERE e.codigo = :codigo"),
    @NamedQuery(name = "Eventos.findByNome", query = "SELECT e FROM Eventos e WHERE e.nome = :nome"),
    @NamedQuery(name = "Eventos.findByDescricao", query = "SELECT e FROM Eventos e WHERE e.descricao = :descricao"),
    @NamedQuery(name = "Eventos.findByIdativo", query = "SELECT e FROM Eventos e WHERE e.idativo = :idativo"),
    @NamedQuery(name = "Eventos.findByDtevento", query = "SELECT e FROM Eventos e WHERE e.dtevento = :dtevento"),
    @NamedQuery(name = "Eventos.findByDthrcadastro", query = "SELECT e FROM Eventos e WHERE e.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "Eventos.findByChaveSeguranca", query = "SELECT e FROM Eventos e WHERE e.chaveSeguranca = :chaveSeguranca")})
public class Eventos implements Serializable {

    @Column(name = "DTFIMEVENTO")
    @Temporal(TemporalType.DATE)
    private Date dtfimevento;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private long codigo;
    @Basic(optional = false)
    @Column(name = "NOME")
    private String nome;
    @Basic(optional = false)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "IDATIVO")
    private Boolean idativo;
    @Basic(optional = false)
    @Column(name = "DTEVENTO")
    @Temporal(TemporalType.DATE)
    private Date dtevento;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @Lob
    @Column(name = "LOGOTIPO")
    private byte[] logotipo;
    @Column(name = "CHAVE_SEGURANCA")
    private String chaveSeguranca;
    @JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEvento")
    private Collection<Vendas> vendasCollection;

    public Eventos() {
    }

    public Eventos(Long id) {
        this.id = id;
    }

    public Eventos(Long id, long codigo, String nome, String descricao, Boolean idativo, Date dtevento, Date dthrcadastro) {
        this.id = id;
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.idativo = idativo;
        this.dtevento = dtevento;
        this.dthrcadastro = dthrcadastro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getIdativo() {
        return idativo;
    }

    public void setIdativo(Boolean idativo) {
        this.idativo = idativo;
    }

    public Date getDtevento() {
        return dtevento;
    }

    public void setDtevento(Date dtevento) {
        this.dtevento = dtevento;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public byte[] getLogotipo() {
        return logotipo;
    }

    public void setLogotipo(byte[] logotipo) {
        this.logotipo = logotipo;
    }    

    public String getChaveSeguranca() {
        return chaveSeguranca;
    }

    public void setChaveSeguranca(String chaveSeguranca) {
        this.chaveSeguranca = chaveSeguranca;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public Collection<Vendas> getVendasCollection() {
        return vendasCollection;
    }

    public void setVendasCollection(Collection<Vendas> vendasCollection) {
        this.vendasCollection = vendasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventos)) {
            return false;
        }
        Eventos other = (Eventos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.Eventos[ id=" + id + " ]";
    }

    public Date getDtfimevento() {
        return dtfimevento;
    }

    public void setDtfimevento(Date dtfimevento) {
        this.dtfimevento = dtfimevento;
    }
    
}
