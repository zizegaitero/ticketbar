/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.eventos.controller;

import com.zizegaitero.ticketbar.cadastros.repository.EmpresaRepository;
import com.zizegaitero.ticketbar.entidades.Eventos;
import com.zizegaitero.ticketbar.entidades.WvendasItens;
import com.zizegaitero.ticketbar.eventos.repository.EventoRepository;
import com.zizegaitero.ticketbar.util.AbstractController;
import com.zizegaitero.ticketbar.util.Printer;
import componentes.Srh.Utils.FormatUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jean
 */
public class EventoController  extends AbstractController {
    
    private final EventoRepository repository;
    private final EmpresaRepository empresaRepository;

    public EventoController() {
        this.repository = new EventoRepository();
        this.empresaRepository = new EmpresaRepository();
    }
    
    public Eventos salvaEventos(Eventos empresa) throws Exception{
        Eventos evento = this.repository.salvaEventos(empresa);
        return evento;
    }
    
    public List montaTabela(Integer casas){
        List<Eventos> findAll = this.repository.findAll();
        List<Object> lista = new ArrayList<>();
        findAll.forEach((cont) -> {
            lista.add(criaListParaTabela(cont,
                    FormatUtil.formataNumero(casas,cont.getCodigo()), 
                    cont.getNome(),                    
                    cont.getIdEmpresa().getNome(),                    
                    FormatUtil.formataData(cont.getDtevento()),
                    FormatUtil.formataData(cont.getDtfimevento()),
                    cont.getIdativo(),
                    FormatUtil.formataDataHora(cont.getDthrcadastro())));
        });    
        return lista;
    }
    
    public Long buscaSequencial() throws Exception {
        return this.repository.buscaProximoSequencial();
    }
    
    public List montaComboEmpresa() throws Exception {
        return notificaCombo(this.empresaRepository.findOnlyAtivo(), "codigo", "nome");
    }
    
    public List montaComboEventosAtivos() throws Exception {
        return notificaCombo(this.repository.findAllAtivos(), "codigo", "nome");
    }
    
    public void geraRelatorioEvento(Eventos evento, Date dataIni, Date dataFim) {
        byte[] logo = null;
        String nomeEmpresa = "";
        List<WvendasItens> findVendasByEvento = this.repository.findVendasByEvento(evento, dataIni, dataFim);
        if(findVendasByEvento != null && !findVendasByEvento.isEmpty()){
             logo = this.repository.findLogoEvento(findVendasByEvento.get(0).getIdevento().longValue());
             nomeEmpresa = this.repository.findNomeEmpresa(findVendasByEvento.get(0).getIdevento().longValue());
        }
        Printer p = new Printer();
        p.imprimeRelatorioEvento(findVendasByEvento, logo, nomeEmpresa);        
    }
    
}
