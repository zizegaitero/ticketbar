/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.vendas.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.Vendas;
import com.zizegaitero.ticketbar.entidades.Vendas_;
import com.zizegaitero.ticketbar.entidades.Eventos;
import com.zizegaitero.ticketbar.entidades.Eventos_;
import com.zizegaitero.ticketbar.entidades.VendasItem;
import com.zizegaitero.ticketbar.entidades.VendasItem_;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class VendasRepository {

    /**
     * Busca as Vendas por evento
     * @param evento
     * @param limite
     * @return 
     */
    public List<Vendas> findAllByEvento(Eventos evento, Integer limite) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Vendas> from = query.from(Vendas.class);
        query.select(from);
        query.where(cb.equal(from.get(Vendas_.idEvento).get(Eventos_.id), evento.getId()),
                cb.notEqual(from.get(Vendas_.idcancelado), Boolean.TRUE));
        query.orderBy(cb.desc(from.get(Vendas_.dthrcadastro)));
        List<Vendas> vendas;
        if(limite != null){
            vendas = em.createQuery(query).setMaxResults(limite).getResultList();
        }else {
            vendas = em.createQuery(query).getResultList();
        }         
        return vendas;
    }
    
    public Long buscaProximoSequencial(Eventos evento) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Vendas> from = query.from(Vendas.class);
        query.select(from);
        query.where(cb.equal(from.get(Vendas_.idEvento).get(Eventos_.id), evento.getId()));
        query.orderBy(cb.desc(from.get(Vendas_.codigo)));
        try {
            Vendas tipoProduto = (Vendas) em.createQuery(query).
                    setMaxResults(1).
                    getSingleResult();
            return tipoProduto.getCodigo() + 1;
        } catch (NoResultException ex) {
            return 1L;
        }
    }
    
    public List<VendasItem> buscaItensVenda(Long idvenda) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<VendasItem> from = query.from(VendasItem.class);
        query.select(from);
        query.where(cb.equal(from.get(VendasItem_.idVenda).get(Vendas_.id), idvenda));
        List<VendasItem> resultList = em.createQuery(query).getResultList();
        return resultList;
    }
    
    

    /**
     * salva uma venda
     *
     * @param venda
     * @return
     * @throws Exception
     */
    public Vendas salvaVenda(Vendas venda) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        venda.setDthrcadastro(new Date());
        venda.setIdcancelado(Boolean.FALSE);
        venda = em.merge(venda);
        em.getTransaction().commit();
        return venda;
    }
    
    /**
     * cancela uma venda
     * 
     * @param venda
     * @return
     * @throws Exception 
     */
    public Vendas cancelaVenda(Vendas venda) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        venda.setDthrcancelado(new Date());
        venda.setIdcancelado(Boolean.TRUE);
        em.merge(venda);
        em.getTransaction().commit();
        return venda;
    }
    
    public void marcaItemImpresso(VendasItem vendasItem) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        vendasItem.setDthrimpresso(new Date());
        vendasItem.setIdImpresso(Boolean.TRUE);
        em.merge(vendasItem);
        em.getTransaction().commit();
    }

    

}
