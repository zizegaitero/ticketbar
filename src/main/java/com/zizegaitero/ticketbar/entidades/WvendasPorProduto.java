/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "WVENDAS_POR_PRODUTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WvendasPorProduto.findAll", query = "SELECT w FROM WvendasPorProduto w"),
    @NamedQuery(name = "WvendasPorProduto.findById", query = "SELECT w FROM WvendasPorProduto w WHERE w.id = :id"),
    @NamedQuery(name = "WvendasPorProduto.findByIdvenda", query = "SELECT w FROM WvendasPorProduto w WHERE w.idvenda = :idvenda"),
    @NamedQuery(name = "WvendasPorProduto.findByCodigo", query = "SELECT w FROM WvendasPorProduto w WHERE w.codigo = :codigo"),
    @NamedQuery(name = "WvendasPorProduto.findByDescricao", query = "SELECT w FROM WvendasPorProduto w WHERE w.descricao = :descricao"),
    @NamedQuery(name = "WvendasPorProduto.findByQuantidade", query = "SELECT w FROM WvendasPorProduto w WHERE w.quantidade = :quantidade"),
    @NamedQuery(name = "WvendasPorProduto.findByValorUnit", query = "SELECT w FROM WvendasPorProduto w WHERE w.valorUnit = :valorUnit"),
    @NamedQuery(name = "WvendasPorProduto.findByValorTotal", query = "SELECT w FROM WvendasPorProduto w WHERE w.valorTotal = :valorTotal"),
    @NamedQuery(name = "WvendasPorProduto.findByDesccombo", query = "SELECT w FROM WvendasPorProduto w WHERE w.desccombo = :desccombo"),
    @NamedQuery(name = "WvendasPorProduto.findByIdevento", query = "SELECT w FROM WvendasPorProduto w WHERE w.idevento = :idevento"),
    @NamedQuery(name = "WvendasPorProduto.findByDescricaoEvento", query = "SELECT w FROM WvendasPorProduto w WHERE w.descricaoEvento = :descricaoEvento"),
    @NamedQuery(name = "WvendasPorProduto.findByDtevento", query = "SELECT w FROM WvendasPorProduto w WHERE w.dtevento = :dtevento"),
    @NamedQuery(name = "WvendasPorProduto.findByDthrcadastro", query = "SELECT w FROM WvendasPorProduto w WHERE w.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "WvendasPorProduto.findByIdvendaItem", query = "SELECT w FROM WvendasPorProduto w WHERE w.idvendaItem = :idvendaItem")})
public class WvendasPorProduto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "ID")
    @Id
    private String id;
    @Column(name = "IDVENDA")
    private BigInteger idvenda;
    @Column(name = "CODIGO")
    private BigInteger codigo;
    @Column(name = "DESCRICAO")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "QUANTIDADE")
    private BigDecimal quantidade;
    @Column(name = "VALOR_UNIT")
    private BigDecimal valorUnit;
    @Column(name = "VALOR_TOTAL")
    private BigDecimal valorTotal;
    @Column(name = "DESCCOMBO")
    private String desccombo;
    @Column(name = "IDEVENTO")
    private BigInteger idevento;
    @Column(name = "DESCRICAO_EVENTO")
    private String descricaoEvento;
    @Column(name = "DTEVENTO")
    @Temporal(TemporalType.DATE)
    private Date dtevento;
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @Column(name = "IDVENDA_ITEM")
    private BigInteger idvendaItem;

    public WvendasPorProduto() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigInteger getIdvenda() {
        return idvenda;
    }

    public void setIdvenda(BigInteger idvenda) {
        this.idvenda = idvenda;
    }

    public BigInteger getCodigo() {
        return codigo;
    }

    public void setCodigo(BigInteger codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorUnit() {
        return valorUnit;
    }

    public void setValorUnit(BigDecimal valorUnit) {
        this.valorUnit = valorUnit;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getDesccombo() {
        return desccombo;
    }

    public void setDesccombo(String desccombo) {
        this.desccombo = desccombo;
    }

    public BigInteger getIdevento() {
        return idevento;
    }

    public void setIdevento(BigInteger idevento) {
        this.idevento = idevento;
    }

    public String getDescricaoEvento() {
        return descricaoEvento;
    }

    public void setDescricaoEvento(String descricaoEvento) {
        this.descricaoEvento = descricaoEvento;
    }

    public Date getDtevento() {
        return dtevento;
    }

    public void setDtevento(Date dtevento) {
        this.dtevento = dtevento;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public BigInteger getIdvendaItem() {
        return idvendaItem;
    }

    public void setIdvendaItem(BigInteger idvendaItem) {
        this.idvendaItem = idvendaItem;
    }
    
}
