/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.controller;

import com.zizegaitero.ticketbar.cadastros.repository.TipoProdutoRepository;
import com.zizegaitero.ticketbar.entidades.TipoProdutos;
import com.zizegaitero.ticketbar.util.AbstractController;
import componentes.Srh.Utils.FormatUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jean
 */
public class TipoProdutoController  extends AbstractController {
    
    private final TipoProdutoRepository repository;

    public TipoProdutoController() {
        this.repository = new TipoProdutoRepository();
    }
    
    public TipoProdutos salvaTipoProduto(TipoProdutos tipoProdutos) throws Exception{
        TipoProdutos salvaTipoProduto = this.repository.salvaTipoProduto(tipoProdutos);
        return salvaTipoProduto;
    }
    
    public List montaTabela(Integer casas){
        List<TipoProdutos> findAll = this.repository.findAll();
        List<Object> lista = new ArrayList<>();
        findAll.forEach((cont) -> {
            lista.add(criaListParaTabela(cont,
                    FormatUtil.formataNumero(casas,cont.getCodigo()), 
                    cont.getDescricao()));
        });    
        return lista;
    }
    
    public Long buscaSequencial() throws Exception {
        return this.repository.buscaProximoSequencial();
    }
    
}
