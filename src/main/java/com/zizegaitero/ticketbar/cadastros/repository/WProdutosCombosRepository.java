/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.WprodutosCombos;
import com.zizegaitero.ticketbar.entidades.WprodutosCombos_;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class WProdutosCombosRepository {

    /**
     * lista todos os tipos de produtos
     *
     * @return
     */
    public List<WprodutosCombos> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<WprodutosCombos> from = query.from(WprodutosCombos.class);
        query.select(from);
        List<WprodutosCombos> wprodutosComboses = em.createQuery(query).getResultList();
        return wprodutosComboses;
    }

    public Long buscaProximoSequencial() throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<WprodutosCombos> from = query.from(WprodutosCombos.class);
        query.select(from);
        query.orderBy(cb.desc(from.get(WprodutosCombos_.codigo)));
        try {
            WprodutosCombos tipoProduto = (WprodutosCombos) em.createQuery(query).
                    setMaxResults(1).
                    getSingleResult();
            return tipoProduto.getCodigo().longValue() + 1;
        } catch (NoResultException ex) {
            return 1L;
        }
    }

    /**
     * Busca somente os produtos ativos
     *
     * @return
     */
    public List<WprodutosCombos> findAllOnlyAtivo() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<WprodutosCombos> from = query.from(WprodutosCombos.class);
        query.select(from);
        query.where(cb.equal(from.get(WprodutosCombos_.idativo), Boolean.TRUE));
        List<WprodutosCombos> wprodutosComboses = em.createQuery(query).getResultList();
        return wprodutosComboses;
    }

    /**
     * retorna um objeto WprodutosCombo baseado em um código
     * se não existir retorna null.
     * @param codigo
     * @return 
     */
    public WprodutosCombos findOneByCodigo(Long codigo) {
        WprodutosCombos wprodutosCombos;
        try {
            EntityManager em = EntityManagerUtil.getEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery query = cb.createQuery();
            Root<WprodutosCombos> from = query.from(WprodutosCombos.class);
            query.select(from);
            query.where(cb.equal(from.get(WprodutosCombos_.idativo), Boolean.TRUE),
                    cb.equal(from.get(WprodutosCombos_.codigo), codigo));
            wprodutosCombos = (WprodutosCombos) em.createQuery(query).getSingleResult();
        } catch (NoResultException ex) {
            wprodutosCombos = null;
        }
        return wprodutosCombos;
    }
    
    
    public WprodutosCombos findOneByComboInfo(BigInteger id) {
        WprodutosCombos wprodutosCombos;
        try {
            EntityManager em = EntityManagerUtil.getEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery query = cb.createQuery();
            Root<WprodutosCombos> from = query.from(WprodutosCombos.class);
            query.select(from);
            query.where(cb.equal(from.get(WprodutosCombos_.idativo), Boolean.TRUE),
                    cb.equal(from.get(WprodutosCombos_.idcomboinfo), id));
            wprodutosCombos = (WprodutosCombos) em.createQuery(query).getSingleResult();
        } catch (NoResultException ex) {
            wprodutosCombos = null;
        }
        return wprodutosCombos;
    }
    
    public WprodutosCombos findOneByProdutosInfo(BigInteger id) {
        WprodutosCombos wprodutosCombos;
        try {
            EntityManager em = EntityManagerUtil.getEntityManager();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery query = cb.createQuery();
            Root<WprodutosCombos> from = query.from(WprodutosCombos.class);
            query.select(from);
            query.where(cb.equal(from.get(WprodutosCombos_.idativo), Boolean.TRUE),
                    cb.equal(from.get(WprodutosCombos_.idprodutoinfo), id));
            wprodutosCombos = (WprodutosCombos) em.createQuery(query).getSingleResult();
        } catch (NoResultException ex) {
            wprodutosCombos = null;
        }
        return wprodutosCombos;
    }

}
