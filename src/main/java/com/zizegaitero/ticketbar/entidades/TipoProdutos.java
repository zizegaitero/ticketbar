/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "TIPO_PRODUTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoProdutos.findAll", query = "SELECT t FROM TipoProdutos t"),
    @NamedQuery(name = "TipoProdutos.findById", query = "SELECT t FROM TipoProdutos t WHERE t.id = :id"),
    @NamedQuery(name = "TipoProdutos.findByCodigo", query = "SELECT t FROM TipoProdutos t WHERE t.codigo = :codigo"),
    @NamedQuery(name = "TipoProdutos.findByDescricao", query = "SELECT t FROM TipoProdutos t WHERE t.descricao = :descricao"),
    @NamedQuery(name = "TipoProdutos.findByDthrcadastro", query = "SELECT t FROM TipoProdutos t WHERE t.dthrcadastro = :dthrcadastro")})
public class TipoProdutos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private long codigo;
    @Basic(optional = false)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoProduto")
    private Collection<Produtos> produtosCollection;

    public TipoProdutos() {
    }

    public TipoProdutos(Long id) {
        this.id = id;
    }

    public TipoProdutos(Long id, long codigo, String descricao, Date dthrcadastro) {
        this.id = id;
        this.codigo = codigo;
        this.descricao = descricao;
        this.dthrcadastro = dthrcadastro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    @XmlTransient
    public Collection<Produtos> getProdutosCollection() {
        return produtosCollection;
    }

    public void setProdutosCollection(Collection<Produtos> produtosCollection) {
        this.produtosCollection = produtosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoProdutos)) {
            return false;
        }
        TipoProdutos other = (TipoProdutos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.TipoProdutos[ id=" + id + " ]";
    }
    
}
