/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "VENDAS_ITEM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VendasItem.findAll", query = "SELECT v FROM VendasItem v"),
    @NamedQuery(name = "VendasItem.findById", query = "SELECT v FROM VendasItem v WHERE v.id = :id"),
    @NamedQuery(name = "VendasItem.findByCodigo", query = "SELECT v FROM VendasItem v WHERE v.codigo = :codigo"),
    @NamedQuery(name = "VendasItem.findByDthrcadastro", query = "SELECT v FROM VendasItem v WHERE v.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "VendasItem.findByQuantidade", query = "SELECT v FROM VendasItem v WHERE v.quantidade = :quantidade"),
    @NamedQuery(name = "VendasItem.findByValor", query = "SELECT v FROM VendasItem v WHERE v.valor = :valor"),
    @NamedQuery(name = "VendasItem.findByIdImpresso", query = "SELECT v FROM VendasItem v WHERE v.idImpresso = :idImpresso"),
    @NamedQuery(name = "VendasItem.findByDthrimpresso", query = "SELECT v FROM VendasItem v WHERE v.dthrimpresso = :dthrimpresso")})
public class VendasItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private long codigo;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "QUANTIDADE")
    private BigDecimal quantidade;
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Basic(optional = false)
    @Column(name = "ID_IMPRESSO")
    private Boolean idImpresso;
    @Column(name = "DTHRIMPRESSO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrimpresso;
    @JoinColumn(name = "ID_COMBO_INFO", referencedColumnName = "ID")
    @ManyToOne
    private ComboInfo idComboInfo;
    @JoinColumn(name = "ID_PRODUTO_INFO", referencedColumnName = "ID")
    @ManyToOne
    private ProdutosInfo idProdutoInfo;
    @JoinColumn(name = "ID_VENDA", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Vendas idVenda;

    public VendasItem() {
    }

    public VendasItem(Long id) {
        this.id = id;
    }

    public VendasItem(Long id, long codigo, Date dthrcadastro, Boolean idImpresso) {
        this.id = id;
        this.codigo = codigo;
        this.dthrcadastro = dthrcadastro;
        this.idImpresso = idImpresso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Boolean getIdImpresso() {
        return idImpresso;
    }

    public void setIdImpresso(Boolean idImpresso) {
        this.idImpresso = idImpresso;
    }

    public Date getDthrimpresso() {
        return dthrimpresso;
    }

    public void setDthrimpresso(Date dthrimpresso) {
        this.dthrimpresso = dthrimpresso;
    }

    public ComboInfo getIdComboInfo() {
        return idComboInfo;
    }

    public void setIdComboInfo(ComboInfo idComboInfo) {
        this.idComboInfo = idComboInfo;
    }

    public ProdutosInfo getIdProdutoInfo() {
        return idProdutoInfo;
    }

    public void setIdProdutoInfo(ProdutosInfo idProdutoInfo) {
        this.idProdutoInfo = idProdutoInfo;
    }

    public Vendas getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(Vendas idVenda) {
        this.idVenda = idVenda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VendasItem)) {
            return false;
        }
        VendasItem other = (VendasItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.VendasItem[ id=" + id + " ]";
    }
    
}
