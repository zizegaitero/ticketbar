/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "COMBO_ITEM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComboItem.findAll", query = "SELECT c FROM ComboItem c"),
    @NamedQuery(name = "ComboItem.findById", query = "SELECT c FROM ComboItem c WHERE c.id = :id"),
    @NamedQuery(name = "ComboItem.findByDthrcadastro", query = "SELECT c FROM ComboItem c WHERE c.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "ComboItem.findByQuantidade", query = "SELECT c FROM ComboItem c WHERE c.quantidade = :quantidade"),
    @NamedQuery(name = "ComboItem.findByValor", query = "SELECT c FROM ComboItem c WHERE c.valor = :valor")})
public class ComboItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "QUANTIDADE")
    private BigDecimal quantidade;
    @Basic(optional = false)
    @Column(name = "VALOR")
    private BigDecimal valor;
    @JoinColumn(name = "ID_COMBO_INFO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ComboInfo idComboInfo;
    @JoinColumn(name = "ID_PRODUTO_INFO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private ProdutosInfo idProdutoInfo;

    public ComboItem() {
    }

    public ComboItem(Long id) {
        this.id = id;
    }

    public ComboItem(Long id, Date dthrcadastro, BigDecimal quantidade, BigDecimal valor) {
        this.id = id;
        this.dthrcadastro = dthrcadastro;
        this.quantidade = quantidade;
        this.valor = valor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public ComboInfo getIdComboInfo() {
        return idComboInfo;
    }

    public void setIdComboInfo(ComboInfo idComboInfo) {
        this.idComboInfo = idComboInfo;
    }

    public ProdutosInfo getIdProdutoInfo() {
        return idProdutoInfo;
    }

    public void setIdProdutoInfo(ProdutosInfo idProdutoInfo) {
        this.idProdutoInfo = idProdutoInfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComboItem)) {
            return false;
        }
        ComboItem other = (ComboItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.ComboItem[ id=" + id + " ]";
    }
    
}
