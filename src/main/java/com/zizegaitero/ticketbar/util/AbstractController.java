/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.util;

import com.zizegaitero.ticketbar.entidades.ProdutosInfo;
import com.zizegaitero.ticketbar.entidades.WprodutosCombos;
import componentes.Srh.Utils.FormatUtil;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author jean
 */
public class AbstractController {
    
    protected List<?> criaListParaTabela(Object objeto, Object... campos) {
        List<Object> lst = new ArrayList<>();
        lst.addAll(Arrays.asList(campos));
        if (objeto != null) {
            lst.add(objeto);
        }
        return lst;
    }
    
    /**
     * monta uma lista para o combo baseado no nome dos campos
     * @param <V>
     * @param lista
     * @param valor
     * @param nome
     * @return
     * @throws Exception 
     */
    protected <V> List notificaCombo(Collection<V> lista, String valor, String... nome) throws Exception{
        String[] valores, nomes;
        Object[] obj;
        List vec;
        try {
            valores = new String[lista.size()];
            nomes = new String[lista.size()];
            obj = lista.toArray();
            int i = 0;
            for (V v : lista) {
                Field f = v.getClass().getDeclaredField(valor);
                f.setAccessible(true);
                valores[i] = f.get(v) + "";
                f = null;
                StringBuilder sb = new StringBuilder();
                for (String n : nome) {
                    f = v.getClass().getDeclaredField(n);
                    f.setAccessible(true);
                    sb.append(" - ").append(f.get(v));
                }
                nomes[i++] = sb.toString().replaceFirst(" - ", "");
                f = null;
            }
            vec = new ArrayList();
            vec.add(valores);
            vec.add(nomes);
            vec.add(obj);
            return vec;
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
            throw ex;
        }
    }
    
    protected List<Object> montaComboProdutosInfo(Collection<ProdutosInfo> produtos){
        List<Object> lista = new LinkedList<Object>();
        String[] valores = new String[produtos.size()];
        String[] nomes = new String[produtos.size()];
        Object[] obj = produtos.toArray();
        Integer i = 0;
        for (ProdutosInfo prod : produtos) {
            valores[i] = FormatUtil.formataNumero(8, prod.getIdProduto().getCodigo());
            nomes[i++] = prod.getIdProduto().getDescricao()+"-"+prod.getIdProduto().getIdTipoProduto().getDescricao();
        }
        lista.add(valores);
        lista.add(nomes);
        lista.add(obj);
        return lista;
    }    
    
    protected List<Object> montaComboWProdutosCombo(Collection<WprodutosCombos> produtos){
        List<Object> lista = new LinkedList<Object>();
        String[] valores = new String[produtos.size()];
        String[] nomes = new String[produtos.size()];
        Object[] obj = produtos.toArray();
        Integer i = 0;
        for (WprodutosCombos prod : produtos) {
            valores[i] = FormatUtil.formataNumero(8, prod.getCodigo().intValue());
            nomes[i++] = prod.getDescricao()+"-"+prod.getTipo();
        }
        lista.add(valores);
        lista.add(nomes);
        lista.add(obj);
        return lista;
    }
    
    
}
