/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "PRODUTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtos.findAll", query = "SELECT p FROM Produtos p"),
    @NamedQuery(name = "Produtos.findById", query = "SELECT p FROM Produtos p WHERE p.id = :id"),
    @NamedQuery(name = "Produtos.findByCodigo", query = "SELECT p FROM Produtos p WHERE p.codigo = :codigo"),
    @NamedQuery(name = "Produtos.findByDescricao", query = "SELECT p FROM Produtos p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "Produtos.findByIdativo", query = "SELECT p FROM Produtos p WHERE p.idativo = :idativo"),
    @NamedQuery(name = "Produtos.findByDthrcadastro", query = "SELECT p FROM Produtos p WHERE p.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "Produtos.findByObservacao", query = "SELECT p FROM Produtos p WHERE p.observacao = :observacao")})
public class Produtos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private long codigo;
    @Basic(optional = false)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "IDATIVO")
    private Boolean idativo;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @Column(name = "OBSERVACAO")
    private String observacao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProduto")
    private Collection<ProdutosInfo> produtosInfoCollection;
    @JoinColumn(name = "ID_TIPO_PRODUTO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private TipoProdutos idTipoProduto;

    public Produtos() {
    }

    public Produtos(Long id) {
        this.id = id;
    }

    public Produtos(Long id, long codigo, String descricao, Boolean idativo, Date dthrcadastro) {
        this.id = id;
        this.codigo = codigo;
        this.descricao = descricao;
        this.idativo = idativo;
        this.dthrcadastro = dthrcadastro;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getIdativo() {
        return idativo;
    }

    public void setIdativo(Boolean idativo) {
        this.idativo = idativo;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public Collection<ProdutosInfo> getProdutosInfoCollection() {
        return produtosInfoCollection;
    }

    public void setProdutosInfoCollection(Collection<ProdutosInfo> produtosInfoCollection) {
        this.produtosInfoCollection = produtosInfoCollection;
    }

    public TipoProdutos getIdTipoProduto() {
        return idTipoProduto;
    }

    public void setIdTipoProduto(TipoProdutos idTipoProduto) {
        this.idTipoProduto = idTipoProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produtos)) {
            return false;
        }
        Produtos other = (Produtos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.Produtos[ id=" + id + " ]";
    }
    
}
