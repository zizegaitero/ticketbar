/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "VENDAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendas.findAll", query = "SELECT v FROM Vendas v"),
    @NamedQuery(name = "Vendas.findById", query = "SELECT v FROM Vendas v WHERE v.id = :id"),
    @NamedQuery(name = "Vendas.findByCodigo", query = "SELECT v FROM Vendas v WHERE v.codigo = :codigo"),
    @NamedQuery(name = "Vendas.findByDthrcadastro", query = "SELECT v FROM Vendas v WHERE v.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "Vendas.findByIdcancelado", query = "SELECT v FROM Vendas v WHERE v.idcancelado = :idcancelado"),
    @NamedQuery(name = "Vendas.findByDthrcancelado", query = "SELECT v FROM Vendas v WHERE v.dthrcancelado = :dthrcancelado"),
    @NamedQuery(name = "Vendas.findByValorVenda", query = "SELECT v FROM Vendas v WHERE v.valorVenda = :valorVenda"),
    @NamedQuery(name = "Vendas.findByValorPagamento", query = "SELECT v FROM Vendas v WHERE v.valorPagamento = :valorPagamento"),
    @NamedQuery(name = "Vendas.findByValorTroco", query = "SELECT v FROM Vendas v WHERE v.valorTroco = :valorTroco")})
public class Vendas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private long codigo;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @Basic(optional = false)
    @Column(name = "IDCANCELADO")
    private Boolean idcancelado;
    @Column(name = "DTHRCANCELADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcancelado;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR_VENDA")
    private BigDecimal valorVenda;
    @Column(name = "VALOR_PAGAMENTO")
    private BigDecimal valorPagamento;
    @Column(name = "VALOR_TROCO")
    private BigDecimal valorTroco;
    @JoinColumn(name = "ID_EVENTO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Eventos idEvento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVenda",fetch = FetchType.EAGER)
    private Collection<VendasItem> vendasItemCollection;

    public Vendas() {
    }

    public Vendas(Long id) {
        this.id = id;
    }

    public Vendas(Long id, long codigo, Date dthrcadastro, Boolean idcancelado) {
        this.id = id;
        this.codigo = codigo;
        this.dthrcadastro = dthrcadastro;
        this.idcancelado = idcancelado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public Boolean getIdcancelado() {
        return idcancelado;
    }

    public void setIdcancelado(Boolean idcancelado) {
        this.idcancelado = idcancelado;
    }

    public Date getDthrcancelado() {
        return dthrcancelado;
    }

    public void setDthrcancelado(Date dthrcancelado) {
        this.dthrcancelado = dthrcancelado;
    }

    public BigDecimal getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }

    public BigDecimal getValorPagamento() {
        return valorPagamento;
    }

    public void setValorPagamento(BigDecimal valorPagamento) {
        this.valorPagamento = valorPagamento;
    }

    public BigDecimal getValorTroco() {
        return valorTroco;
    }

    public void setValorTroco(BigDecimal valorTroco) {
        this.valorTroco = valorTroco;
    }

    public Eventos getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Eventos idEvento) {
        this.idEvento = idEvento;
    }

    @XmlTransient
    public Collection<VendasItem> getVendasItemCollection() {
        return vendasItemCollection;
    }

    public void setVendasItemCollection(Collection<VendasItem> vendasItemCollection) {
        this.vendasItemCollection = vendasItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendas)) {
            return false;
        }
        Vendas other = (Vendas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.Vendas[ id=" + id + " ]";
    }
    
}
