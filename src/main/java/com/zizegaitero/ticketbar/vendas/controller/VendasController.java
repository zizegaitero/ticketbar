/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.vendas.controller;

import com.zizegaitero.ticketbar.cadastros.repository.ComboRepository;
import com.zizegaitero.ticketbar.cadastros.repository.ProdutosRepository;
import com.zizegaitero.ticketbar.cadastros.repository.WProdutosCombosRepository;
import com.zizegaitero.ticketbar.entidades.Eventos;
import com.zizegaitero.ticketbar.entidades.Vendas;
import com.zizegaitero.ticketbar.entidades.VendasItem;
import com.zizegaitero.ticketbar.entidades.WprodutosCombos;
import com.zizegaitero.ticketbar.eventos.repository.EventoRepository;
import com.zizegaitero.ticketbar.util.AbstractController;
import com.zizegaitero.ticketbar.util.Printer;
import com.zizegaitero.ticketbar.vendas.repository.VendasRepository;
import componentes.Srh.Utils.FormatUtil;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jean
 */
public class VendasController extends AbstractController {

    private final VendasRepository repository;
    private final ProdutosRepository produtosRepository;
    private final ComboRepository comboRepository;
    private final EventoRepository eventoRepository;
    private final WProdutosCombosRepository wProdutosCombosRepository;

    public VendasController() {
        this.repository = new VendasRepository();
        this.produtosRepository = new ProdutosRepository();
        this.comboRepository = new ComboRepository();
        this.eventoRepository = new EventoRepository();
        this.wProdutosCombosRepository = new WProdutosCombosRepository();
    }

    public Vendas salvaVenda(Vendas vendas, List<VendasItemObject> lista) throws Exception {
        List<VendasItem> itens = new ArrayList<>();
        VendasItem item;
        for (VendasItemObject vendasItems : lista) {
            item = new VendasItem();
            item.setCodigo(vendasItems.getSequencial());
            item.setDthrcadastro(new Date());
            if (vendasItems.getWprodutosCombos().getIdcomboinfo() != null) {
                item.setIdComboInfo(comboRepository.findOneInfo(vendasItems.getWprodutosCombos().getIdcomboinfo().longValue()));
            }
            if (vendasItems.getWprodutosCombos().getIdprodutoinfo() != null) {
                item.setIdProdutoInfo(produtosRepository.findOneInfo(vendasItems.getWprodutosCombos().getIdprodutoinfo().longValue()));
            }
            item.setIdVenda(vendas);
            item.setQuantidade(vendasItems.getQuantidade());
            item.setValor(vendasItems.getValor());
            item.setIdImpresso(Boolean.FALSE);
            itens.add(item);
        }
        vendas.setVendasItemCollection(itens);
        vendas = this.repository.salvaVenda(vendas);
        this.imprimeVenda(vendas.getId());
        return vendas;
    }

    public Long buscaSequencial(Eventos eve) throws Exception {
        Long prod = this.repository.buscaProximoSequencial(eve);
        return prod;
    }

    public List montaTabelaItens(Collection<VendasItemObject> itens) {
        List<Object> lista = new ArrayList<>();
        for (VendasItemObject cont : itens) {
            lista.add(criaListParaTabela(cont,
                    cont.getSequencial() + "-"
                    + cont.getWprodutosCombos().getDescricao(),
                    FormatUtil.formatNumber(cont.getQuantidade()),
                    FormatUtil.formatNumber(cont.getValor()),
                    FormatUtil.formatNumber(cont.getQuantidade().multiply(cont.getValor()))
            ));
        }
        return lista;
    }

    public WprodutosCombos buscaPorCodigo(Long codigo) throws Exception {
        return this.wProdutosCombosRepository.findOneByCodigo(codigo);
    }

    public List montaComboProdutos() throws Exception {
        return montaComboWProdutosCombo(this.wProdutosCombosRepository.findAllOnlyAtivo());
    }

    public WprodutosCombos buscaWPorProdutoInfo(Long codigo) throws Exception {
        return this.wProdutosCombosRepository.findOneByProdutosInfo(BigInteger.valueOf(codigo));
    }

    public WprodutosCombos buscaWPorComboInfo(Long codigo) throws Exception {
        return this.wProdutosCombosRepository.findOneByComboInfo(BigInteger.valueOf(codigo));
    }

    public void imprimeVenda(Long idvenda) {
        List<VendasItemRelatorioObject> lista = new ArrayList<>();
        try {
            List<VendasItem> buscaItensVenda = this.repository.buscaItensVenda(idvenda);
            VendasItemRelatorioObject objeto;
            for (VendasItem vendasItem : buscaItensVenda) {
                for (int i = 0; i < vendasItem.getQuantidade().intValue(); i++) {
                    objeto = new VendasItemRelatorioObject();
                    objeto.setChave(vendasItem.getIdVenda().getIdEvento().getChaveSeguranca());
                    objeto.setDataVenda(vendasItem.getDthrcadastro());
                    objeto.setEvento(vendasItem.getIdVenda().getIdEvento().getNome());
                    objeto.setIdvendaItem(vendasItem.getId());
                    objeto.setEmpresa(vendasItem.getIdVenda().getIdEvento().getIdEmpresa().getNome());
                    objeto.setLogotipo(eventoRepository.findLogoEvento(vendasItem.getIdVenda().getIdEvento().getId()));
                    objeto.setProduto(vendasItem.getIdComboInfo() != null
                            ? vendasItem.getIdComboInfo().getIdCombo().getDescricao().trim()
                            : vendasItem.getIdProdutoInfo().getIdProduto().getDescricao().trim()
                            );
                    objeto.setValor(vendasItem.getValor());
                    lista.add(objeto);
                }
               this.repository.marcaItemImpresso(vendasItem);
            }
            Printer p = new Printer();
            p.imprimeTicket(lista);
        } catch (Exception ex) {
            Logger.getLogger(VendasController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
