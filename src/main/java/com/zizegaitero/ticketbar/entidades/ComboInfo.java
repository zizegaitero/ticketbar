/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "COMBO_INFO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComboInfo.findAll", query = "SELECT c FROM ComboInfo c"),
    @NamedQuery(name = "ComboInfo.findById", query = "SELECT c FROM ComboInfo c WHERE c.id = :id"),
    @NamedQuery(name = "ComboInfo.findByValor", query = "SELECT c FROM ComboInfo c WHERE c.valor = :valor"),
    @NamedQuery(name = "ComboInfo.findByIdativo", query = "SELECT c FROM ComboInfo c WHERE c.idativo = :idativo"),
    @NamedQuery(name = "ComboInfo.findByDthrcadastro", query = "SELECT c FROM ComboInfo c WHERE c.dthrcadastro = :dthrcadastro")})
public class ComboInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Basic(optional = false)
    @Column(name = "IDATIVO")
    private Boolean idativo;
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @OneToMany(mappedBy = "idComboInfo")
    private Collection<VendasItem> vendasItemCollection;
    @JoinColumn(name = "ID_COMBO", referencedColumnName = "ID")
    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Combo idCombo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idComboInfo")
    private Collection<ComboItem> comboItemCollection;

    public ComboInfo() {
    }

    public ComboInfo(Long id) {
        this.id = id;
    }

    public ComboInfo(Long id, Boolean idativo) {
        this.id = id;
        this.idativo = idativo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Boolean getIdativo() {
        return idativo;
    }

    public void setIdativo(Boolean idativo) {
        this.idativo = idativo;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    @XmlTransient
    public Collection<VendasItem> getVendasItemCollection() {
        return vendasItemCollection;
    }

    public void setVendasItemCollection(Collection<VendasItem> vendasItemCollection) {
        this.vendasItemCollection = vendasItemCollection;
    }

    public Combo getIdCombo() {
        return idCombo;
    }

    public void setIdCombo(Combo idCombo) {
        this.idCombo = idCombo;
    }

    @XmlTransient
    public Collection<ComboItem> getComboItemCollection() {
        return comboItemCollection;
    }

    public void setComboItemCollection(Collection<ComboItem> comboItemCollection) {
        this.comboItemCollection = comboItemCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComboInfo)) {
            return false;
        }
        ComboInfo other = (ComboInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.ComboInfo[ id=" + id + " ]";
    }
    
}
