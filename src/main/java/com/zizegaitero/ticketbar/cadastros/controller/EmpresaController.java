/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.controller;

import com.zizegaitero.ticketbar.cadastros.repository.EmpresaRepository;
import com.zizegaitero.ticketbar.entidades.Empresa;
import com.zizegaitero.ticketbar.util.AbstractController;
import componentes.Srh.Utils.FormatUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jean
 */
public class EmpresaController  extends AbstractController {
    
    private final EmpresaRepository repository;

    public EmpresaController() {
        this.repository = new EmpresaRepository();
    }
    
    public Empresa salvaEmpresa(Empresa empresa) throws Exception{
        Empresa salvaTipoProduto = this.repository.salvaEmpresa(empresa);
        return salvaTipoProduto;
    }
    
    public List montaTabela(Integer casas){
        List<Empresa> findAll = this.repository.findAll();
        List<Object> lista = new ArrayList<>();
        findAll.forEach((cont) -> {
            lista.add(criaListParaTabela(cont,
                    FormatUtil.formataNumero(casas,cont.getCodigo()), 
                    cont.getNome(),
                    cont.getIdativo(),
                    FormatUtil.formataDataHora(cont.getDthrcadastro())));
        });    
        return lista;
    }
    
    public Long buscaSequencial() throws Exception {
        return this.repository.buscaProximoSequencial();
    }
    
}
