/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jean
 */
@Entity
@Table(name = "COMBO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Combo.findAll", query = "SELECT c FROM Combo c"),
    @NamedQuery(name = "Combo.findById", query = "SELECT c FROM Combo c WHERE c.id = :id"),
    @NamedQuery(name = "Combo.findByCodigo", query = "SELECT c FROM Combo c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "Combo.findByDescricao", query = "SELECT c FROM Combo c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "Combo.findByDthrcadastro", query = "SELECT c FROM Combo c WHERE c.dthrcadastro = :dthrcadastro"),
    @NamedQuery(name = "Combo.findByIdativo", query = "SELECT c FROM Combo c WHERE c.idativo = :idativo")})
public class Combo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private long codigo;
    @Basic(optional = false)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "DTHRCADASTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dthrcadastro;
    @Basic(optional = false)
    @Column(name = "IDATIVO")
    private Boolean idativo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCombo")
    private Collection<ComboInfo> comboInfoCollection;

    public Combo() {
    }

    public Combo(Long id) {
        this.id = id;
    }

    public Combo(Long id, long codigo, String descricao, Date dthrcadastro, Boolean idativo) {
        this.id = id;
        this.codigo = codigo;
        this.descricao = descricao;
        this.dthrcadastro = dthrcadastro;
        this.idativo = idativo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDthrcadastro() {
        return dthrcadastro;
    }

    public void setDthrcadastro(Date dthrcadastro) {
        this.dthrcadastro = dthrcadastro;
    }

    public Boolean getIdativo() {
        return idativo;
    }

    public void setIdativo(Boolean idativo) {
        this.idativo = idativo;
    }

    @XmlTransient
    public Collection<ComboInfo> getComboInfoCollection() {
        return comboInfoCollection;
    }

    public void setComboInfoCollection(Collection<ComboInfo> comboInfoCollection) {
        this.comboInfoCollection = comboInfoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Combo)) {
            return false;
        }
        Combo other = (Combo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zizegaitero.ticketbar.entidades.Combo[ id=" + id + " ]";
    }
    
}
