/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.vendas.controller;

import com.zizegaitero.ticketbar.entidades.WprodutosCombos;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 *
 * @author jean
 */
public class VendasItemObject implements Serializable{ 
    
    private Long codigo;
    private Long sequencial;
    private BigDecimal quantidade;
    private BigDecimal valor;
    private WprodutosCombos wprodutosCombos;

    public VendasItemObject() {
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public WprodutosCombos getWprodutosCombos() {
        return wprodutosCombos;
    }

    public void setWprodutosCombos(WprodutosCombos wprodutosCombos) {
        this.wprodutosCombos = wprodutosCombos;
    }

    public Long getSequencial() {
        return sequencial;
    }

    public void setSequencial(Long sequencial) {
        this.sequencial = sequencial;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.codigo);
        hash = 71 * hash + Objects.hashCode(this.sequencial);
        hash = 71 * hash + Objects.hashCode(this.quantidade);
        hash = 71 * hash + Objects.hashCode(this.valor);
        hash = 71 * hash + Objects.hashCode(this.wprodutosCombos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VendasItemObject other = (VendasItemObject) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.sequencial, other.sequencial)) {
            return false;
        }
        if (!Objects.equals(this.quantidade, other.quantidade)) {
            return false;
        }
        if (!Objects.equals(this.valor, other.valor)) {
            return false;
        }
        if (!Objects.equals(this.wprodutosCombos, other.wprodutosCombos)) {
            return false;
        }
        return true;
    }
    
    

    
}
