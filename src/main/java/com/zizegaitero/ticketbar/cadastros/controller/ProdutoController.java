/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.controller;

import com.zizegaitero.ticketbar.cadastros.repository.ProdutosRepository;
import com.zizegaitero.ticketbar.cadastros.repository.TipoProdutoRepository;
import com.zizegaitero.ticketbar.cadastros.repository.WProdutosCombosRepository;
import com.zizegaitero.ticketbar.entidades.ProdutosInfo;
import com.zizegaitero.ticketbar.util.AbstractController;
import componentes.Srh.Utils.FormatUtil;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jean
 */
public class ProdutoController  extends AbstractController {
    
    private final ProdutosRepository repository;
    private final TipoProdutoRepository tipoProdutosRepository;
    private final WProdutosCombosRepository wProdutosCombosRepository;

    public ProdutoController() {
        this.repository = new ProdutosRepository();
        this.tipoProdutosRepository = new TipoProdutoRepository();
        this.wProdutosCombosRepository = new WProdutosCombosRepository();
    }
    
    public ProdutosInfo salvaProdutos(ProdutosInfo prodi) throws Exception{
        prodi = this.repository.salvaProdutos(prodi);
        return prodi;
    }
    
    public List montaTabela(Integer casas){
        List<ProdutosInfo> findAll = this.repository.findAll();
        List<Object> lista = new ArrayList<>();
        findAll.forEach((cont) -> {
            lista.add(criaListParaTabela(cont,
                    FormatUtil.formataNumero(casas,cont.getIdProduto().getCodigo()), 
                    cont.getIdProduto().getDescricao(),                    
                    cont.getIdProduto().getIdTipoProduto().getCodigo()+"-"+cont.getIdProduto().getIdTipoProduto().getDescricao(),                    
                    cont.getValor(),
                    cont.getIdProduto().getIdativo(),
                    FormatUtil.formataDataHora(cont.    getDthrcadastro())));
        });    
        return lista;
    }
    
    public Long buscaSequencial() throws Exception {
        Long prod = this.wProdutosCombosRepository.buscaProximoSequencial();
        return prod;
    }
    
    public List montaComboTipo() throws Exception {
        return notificaCombo(this.tipoProdutosRepository.findAll(), "codigo", "descricao");
    }
    
}
