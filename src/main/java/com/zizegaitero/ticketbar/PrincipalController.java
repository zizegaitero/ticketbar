/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar;

import com.zizegaitero.ticketbar.entidades.Eventos;
import com.zizegaitero.ticketbar.entidades.Vendas;
import com.zizegaitero.ticketbar.eventos.repository.EventoRepository;
import com.zizegaitero.ticketbar.util.AbstractController;
import com.zizegaitero.ticketbar.util.JInternalFrameUtil;
import com.zizegaitero.ticketbar.vendas.repository.VendasRepository;
import componentes.Srh.JButton.SrhButtonLink;
import componentes.Srh.Utils.FormatUtil;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 *
 * @author jean
 */
public class PrincipalController extends AbstractController{

    private final JDesktopPane desktopPane;
    private final EventoRepository eventoRepository;
    private final VendasRepository vendasRepository;

    public PrincipalController(JDesktopPane desktopPane) {
        this.desktopPane = desktopPane;
        this.eventoRepository = new EventoRepository();
        this.vendasRepository = new VendasRepository();
    }
    
    public Eventos buscaEventoAtivo(){
        return this.eventoRepository.findEventoAtivoVenda();
    }

    /**
     * adiciona uma tela no desktop.
     *
     * @param frame
     */
    public void addFrame(JInternalFrameUtil frame) {

        //sob.setFrameIcon(new ImageIcon(getIconImage()));
        boolean exists = false;
        for (JInternalFrame jit : desktopPane.getAllFrames()) {
            if (jit.getClass().getName().equals(frame.getClass().getName())) {
                frame = (JInternalFrameUtil) jit;
                exists = true;
                break;
            }
        }
        if (!exists) {
            desktopPane.add(frame);
            frame.setVisible(true);            
        }
        frame.moveToFront();
        desktopPane.moveToFront(frame);
        try {
            frame.setMaximum(frame.getMaximizado());
            frame.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
    }
    
    public List buscaVendas(Eventos eve){
        return this.vendasRepository.findAllByEvento(eve, null);
    }
    
    public List montaTabelaItens(List<Vendas> itens){        
        List<Object> lista = new ArrayList<>();
        for (Vendas cont : itens) {
            lista.add(criaListParaTabela(cont,
                    cont.getCodigo(),
                    FormatUtil.formataData(cont.getDthrcadastro(), "dd - HH:mm"),
                    FormatUtil.formatNumber(cont.getValorVenda())
                    ));
        }   
        return lista;
    }
    
    public void cancelaVenda(Vendas vendas) throws Exception{
        this.vendasRepository.cancelaVenda(vendas);
    }
}
