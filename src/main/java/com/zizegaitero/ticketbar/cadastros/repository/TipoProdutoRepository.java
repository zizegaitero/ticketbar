/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.cadastros.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.TipoProdutos;
import com.zizegaitero.ticketbar.entidades.TipoProdutos_;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class TipoProdutoRepository {

    /**
     * lista todos os tipos de produtos
     *
     * @return
     */
    public List<TipoProdutos> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<TipoProdutos> from = query.from(TipoProdutos.class);
        query.select(from);
        List<TipoProdutos> tipoProdutos = em.createQuery(query).getResultList();
        return tipoProdutos;
    }

    /**
     * salva um tipo de produto
     *
     * @param tipoProdutos
     * @return
     * @throws Exception
     */
    public TipoProdutos salvaTipoProduto(TipoProdutos tipoProdutos) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        tipoProdutos.setDthrcadastro(new Date());
        em.merge(tipoProdutos);
        em.getTransaction().commit();
        return tipoProdutos;
    }

    /**
     * Busca o próximo código
     * @return
     * @throws Exception 
     */
    public Long buscaProximoSequencial() throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<TipoProdutos> from = query.from(TipoProdutos.class);
        query.select(from);
        query.orderBy(cb.desc(from.get(TipoProdutos_.codigo)));
        try {
            TipoProdutos tipoProduto = (TipoProdutos) em.createQuery(query).
                    setMaxResults(1).
                    getSingleResult();
            return tipoProduto.getCodigo() + 1;
        } catch (NoResultException ex) {
            return 1L;
        }
    }

}
