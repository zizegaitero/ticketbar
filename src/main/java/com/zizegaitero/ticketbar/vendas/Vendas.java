/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.vendas;

import com.zizegaitero.ticketbar.Principal;
import com.zizegaitero.ticketbar.entidades.VendasItem;
import com.zizegaitero.ticketbar.entidades.WprodutosCombos;
import com.zizegaitero.ticketbar.util.JInternalFrameUtil;
import com.zizegaitero.ticketbar.vendas.controller.VendasController;
import com.zizegaitero.ticketbar.vendas.controller.VendasItemObject;
import componentes.Srh.JNumberFields.SrhJNumberFields;
import componentes.Srh.JTable.dsl.TableDecorator;
import componentes.Srh.Utils.FormatUtil;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

/**
 *
 * @author jean
 */
public class Vendas extends JInternalFrameUtil {

    private Integer COUNT = 0;
    private final Integer DESCRICAO_COLUMN = COUNT++;
    private final Integer QTD_COLUMN = COUNT++;
    private final Integer VALOR_COLUMN = COUNT++;
    private final Integer TOTAL_COLUMN = COUNT++;
    private final Integer OBJETO_COLUMN = COUNT++;
    private final VendasController vendasController;
    private com.zizegaitero.ticketbar.entidades.Vendas objeto;
    private List<VendasItemObject> itens;
    private boolean vender = false;

    /**
     * Creates new form TipoProduto
     *
     * @param principal
     * @param telaPai
     * @param objeto
     */
    public Vendas(Principal principal, com.zizegaitero.ticketbar.entidades.Vendas objeto) {
        initComponents();
        this.principal = principal;
        this.objeto = objeto;
        this.setTitle("Venda de Itens");
        this.vendasController = new VendasController();
        new TableDecorator(lista).onColumn(OBJETO_COLUMN).hideColumn().apply().
                onColumn(QTD_COLUMN).asNumber().withCasasDecimais(SrhJNumberFields.CasasDecimais.DUAS).apply().apply().
                onColumn(VALOR_COLUMN).asNumber().withCasasDecimais(SrhJNumberFields.CasasDecimais.DUAS).apply().apply().
                onColumn(TOTAL_COLUMN).asNumber().withCasasDecimais(SrhJNumberFields.CasasDecimais.DUAS).apply().apply();
        this.carregar();
        novaVenda();
        this.idProduto_item.getEdit().addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void focusLost(FocusEvent e) {
                WprodutosCombos objetoSelecionado = (WprodutosCombos) idProduto_item.getObjetoSelecionado();
                montaObjetoVenda(objetoSelecionado);
            }
        });
        this.setVender();
        this.setFechar();
        this.limpar();
    }

    public void carregaTabela() {
        try {
            venda_label.setText(this.objeto.getCodigo() + "");
            List list = this.vendasController.montaTabelaItens(itens);
            lista.insereTabela(list);
        } catch (Exception e) {
            this.principal.mostraMensagemErro(e.getMessage());
        }
    }

    private void salvar() {
        if (codigo_item.Valida() && idProduto_item.valida()
                && quantidade_item.ValidaMaiorQueZero() && valor_item.ValidaMaiorQueZero()) {
            try {
                VendasItemObject pro = new VendasItemObject();
                pro.setSequencial(itens.size() + 1L);
                pro.setCodigo(codigo_item.getValorLong());
                pro.setQuantidade(quantidade_item.getBigValor());
                pro.setValor(valor_item.getBigValor());
                pro.setWprodutosCombos((WprodutosCombos) idProduto_item.getObjetoSelecionado());

                itens.add(pro);
                this.limpar();
                this.carregaTabela();

            } catch (Exception ex) {
                Logger.getLogger(Vendas.class.getName()).log(Level.SEVERE, null, ex);
                this.principal.mostraMensagemErro(ex.getMessage());
            }
        }
        calculaTotal(itens);
    }

    private void vender() {
        this.vender = true;
        this.ajustaComponentes(this.vender);
    }

    private void ajustaComponentes(Boolean vender) {
        valor_pago.setEditable(vender);
        codigo_item.setEditable(!vender);
        idProduto_item.setEditable(!vender);
        quantidade_item.setEditable(!vender);
        if (!vender) {
            limpar();
        } else {
            valor_pago.grabFocus();
        }
    }

    private void limpar() {
        montaObjetoVenda(null);
        codigo_item.grabFocus();
    }

    private void carregar() {
        try {
            List tipos = this.vendasController.montaComboProdutos();
            idProduto_item.setValor(tipos, true, true);
        } catch (Exception e) {
            this.principal.mostraMensagemErro(e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        idProduto_item = new componentes.Srh.JComboBoxs.SrhJComboBox();
        codigo_item = new componentes.Srh.JTextFields.SrhJTextField();
        jLabel3 = new javax.swing.JLabel();
        quantidade_item = new componentes.Srh.JNumberFields.SrhJNumberFields();
        jLabel7 = new javax.swing.JLabel();
        valor_item = new componentes.Srh.JNumberFields.SrhJNumberFields();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista = new componentes.Srh.JTable.SrhTable();
        total_items = new componentes.Srh.JNumberFields.SrhJNumberFields();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        valor_pago = new componentes.Srh.JNumberFields.SrhJNumberFields();
        jLabel6 = new javax.swing.JLabel();
        troco = new componentes.Srh.JNumberFields.SrhJNumberFields();
        jLabel9 = new javax.swing.JLabel();
        venda_label = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        srhButtonLink1 = new componentes.Srh.JButton.SrhButtonLink();
        srhButtonLink2 = new componentes.Srh.JButton.SrhButtonLink();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Venda"));

        jLabel1.setText("Item:");

        jLabel2.setText("Quantidade:");

        idProduto_item.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        idProduto_item.setPesquisavel(java.lang.Boolean.TRUE);

        codigo_item.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        codigo_item.setLpadzero(java.lang.Boolean.TRUE);
        codigo_item.setMaxLength(new java.lang.Integer(8));
        codigo_item.setNumero(java.lang.Boolean.TRUE);
        codigo_item.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                codigo_itemFocusLost(evt);
            }
        });

        jLabel3.setText("Código:");

        quantidade_item.setText("srhJNumberFields1");
        quantidade_item.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        quantidade_item.setMoeda(true);
        quantidade_item.setRequerido(java.lang.Boolean.TRUE);

        jLabel7.setText("Valor:");

        valor_item.setEditable(false);
        valor_item.setText("srhJNumberFields1");
        valor_item.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        valor_item.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                valor_itemFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(idProduto_item, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                    .addComponent(codigo_item, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quantidade_item, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(valor_item, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codigo_item, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idProduto_item, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(quantidade_item, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valor_item, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addContainerGap(113, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Items"));

        lista.setColunas(new String[] {"Produto", "Quantidade", "Valor", "Total", "Objeto"});
        jScrollPane1.setViewportView(lista);

        total_items.setEditable(false);
        total_items.setForeground(new java.awt.Color(153, 0, 0));
        total_items.setText("srhJNumberFields1");
        total_items.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        total_items.setMoeda(true);

        jLabel4.setText("Total dos Itens:");

        jLabel5.setText("Valor Pago:");

        valor_pago.setEditable(false);
        valor_pago.setText("srhJNumberFields1");
        valor_pago.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        valor_pago.setMoeda(true);

        jLabel6.setText("Troco:");

        troco.setEditable(false);
        troco.setForeground(new java.awt.Color(0, 153, 0));
        troco.setText("srhJNumberFields1");
        troco.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        troco.setMoeda(true);
        troco.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                trocoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                trocoFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)))
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(total_items, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(valor_pago, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(troco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(total_items, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valor_pago, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(troco, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel9.setText("Venda Nº:");
        jLabel9.setFocusable(false);

        venda_label.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        venda_label.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        venda_label.setText("1");
        venda_label.setFocusable(false);

        jLabel10.setText("Atalhos:");
        jLabel10.setFocusable(false);

        srhButtonLink1.setText("( * ) - Fechar Venda");
        srhButtonLink1.setFocusable(false);

        srhButtonLink2.setText("( / ) - Limpar");
        srhButtonLink2.setFocusable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(venda_label, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(srhButtonLink1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(srhButtonLink2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(venda_label, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(srhButtonLink1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(srhButtonLink2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void codigo_itemFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codigo_itemFocusLost
        Long valorInt = codigo_item.getValorLong();
        if (valorInt != null) {
            try {
                buscaItemCodigo(valorInt);
            } catch (Exception ex) {
                Logger.getLogger(Vendas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_codigo_itemFocusLost

    private void valor_itemFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor_itemFocusGained
        salvar();
    }//GEN-LAST:event_valor_itemFocusGained

    private void trocoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_trocoFocusLost
        fecharVenda();
    }//GEN-LAST:event_trocoFocusLost

    private void trocoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_trocoFocusGained
        calculaTroco();
    }//GEN-LAST:event_trocoFocusGained

    private void montaObejtoTela(com.zizegaitero.ticketbar.entidades.Vendas objeto) {
        montaObjetoVenda(null);
        this.objeto = objeto;
        if (this.objeto == null) {
            try {
                this.objeto = new com.zizegaitero.ticketbar.entidades.Vendas();
                Long buscaSequencial = this.vendasController.buscaSequencial(this.principal.getEventoAtivo());
                this.objeto.setCodigo(buscaSequencial);
                valor_pago.setBigValor(BigDecimal.ZERO);
                troco.setBigValor(BigDecimal.ZERO);
            } catch (Exception ex) {
                Logger.getLogger(Vendas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (this.objeto.getVendasItemCollection() != null) {
            for (VendasItem vendasItem : this.objeto.getVendasItemCollection()) {
                VendasItemObject pro = new VendasItemObject();
                pro.setSequencial(vendasItem.getCodigo());
                pro.setQuantidade(vendasItem.getQuantidade());
                pro.setValor(vendasItem.getValor());
                try {
                    if (vendasItem.getIdProdutoInfo() != null) {
                        pro.setWprodutosCombos(this.vendasController.buscaWPorProdutoInfo(vendasItem.getIdProdutoInfo().getId()));
                    } else if (vendasItem.getIdComboInfo() != null) {
                        pro.setWprodutosCombos(this.vendasController.buscaWPorComboInfo(vendasItem.getIdComboInfo().getId()));
                    } else {
                        this.principal.mostraMensagemErro("Não tem vinculo com produto ou combo");
                    }
                    itens.add(pro);
                } catch (Exception ex) {
                    Logger.getLogger(Vendas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            valor_pago.setBigValor(this.objeto.getValorPagamento());
            troco.setBigValor(this.objeto.getValorTroco());
        }
        this.carregaTabela();
        calculaTotal(itens);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private componentes.Srh.JTextFields.SrhJTextField codigo_item;
    private componentes.Srh.JComboBoxs.SrhJComboBox idProduto_item;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private componentes.Srh.JTable.SrhTable lista;
    private componentes.Srh.JNumberFields.SrhJNumberFields quantidade_item;
    private componentes.Srh.JButton.SrhButtonLink srhButtonLink1;
    private componentes.Srh.JButton.SrhButtonLink srhButtonLink2;
    private componentes.Srh.JNumberFields.SrhJNumberFields total_items;
    private componentes.Srh.JNumberFields.SrhJNumberFields troco;
    private componentes.Srh.JNumberFields.SrhJNumberFields valor_item;
    private componentes.Srh.JNumberFields.SrhJNumberFields valor_pago;
    private javax.swing.JLabel venda_label;
    // End of variables declaration//GEN-END:variables

    private void calculaTotal(Collection<VendasItemObject> itens) {
        BigDecimal tot = BigDecimal.ZERO;
        for (VendasItemObject iten : itens) {
            tot = tot.add(iten.getValor().multiply(iten.getQuantidade()));
        }
        this.total_items.setBigValor(tot);
    }

    private void calculaTroco() {
        if (valor_pago.ValidaMaiorQueZero() && total_items.ValidaMaiorQueZero() && valor_pago.getBigValor().compareTo(total_items.getBigValor()) >= 0) {
            troco.setBigValor(valor_pago.getBigValor().subtract(total_items.getBigValor()));
        } else {
            troco.setBigValor(BigDecimal.ZERO);
        }
    }

    private void buscaItemCodigo(Long codigo) throws Exception {
        WprodutosCombos buscaPorCodigo = this.vendasController.buscaPorCodigo(codigo);
        montaObjetoVenda(buscaPorCodigo);
    }

    private void montaObjetoVenda(WprodutosCombos objeto) {
        if (objeto == null) {
            codigo_item.setText("");
            idProduto_item.LimparComponente();
            quantidade_item.setBigValor(BigDecimal.ZERO);
            valor_item.setBigValor(BigDecimal.ZERO);
        } else {
            codigo_item.setText(FormatUtil.formataNumero(8, objeto.getCodigo().intValue()));
            idProduto_item.setObjetoSelecionado(objeto);
            quantidade_item.setBigValor(BigDecimal.ONE);
            valor_item.setBigValor(objeto.getValor());
            quantidade_item.grabFocus();
        }
    }

    public final void setVender() {
        this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_MULTIPLY,0), "Vender");
        VenderAction acaoSalvar = new VenderAction("Vender");
        this.getActionMap().put("Vender", acaoSalvar);
    }

    public final void setFechar() {
        this.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_DIVIDE,0), "Fechar");
        NovaVenda acaoSalvar = new NovaVenda("Fechar");
        this.getActionMap().put("Fechar", acaoSalvar);
    }

    private class VenderAction extends AbstractAction {

        public VenderAction(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (isSelected()) {
                vender();
            }
        }
    }

    private class NovaVenda extends AbstractAction {

        public NovaVenda(String name) {
            super(name);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (isSelected()) {
                novaVenda();
            }
        }
    }

    public void fecharVenda() {
        if (this.vender && total_items.ValidaMaiorQueZero() && valor_pago.ValidaMaiorQueZero()) {
            if (valor_pago.getBigValor().compareTo(total_items.getBigValor()) < 0) {
                valor_pago.setBalloonMessage("O valor pago não pode ser menor que o valor dos Itens");
                valor_pago.showBalloonTip();
                return;
            }
            try {
                if (this.objeto == null) {
                    this.objeto = new com.zizegaitero.ticketbar.entidades.Vendas();
                }
                this.objeto.setCodigo(Long.parseLong(venda_label.getText()));
                this.objeto.setValorPagamento(valor_pago.getBigValor());
                this.objeto.setValorTroco(troco.getBigValor());
                this.objeto.setValorVenda(total_items.getBigValor());
                this.objeto.setIdEvento(this.principal.getEventoAtivo());
                com.zizegaitero.ticketbar.entidades.Vendas salvaVenda = this.vendasController.salvaVenda(objeto, itens);
                novaVenda();
            } catch (Exception ex) {
                this.principal.mostraMensagemErro(ex.getMessage());
            }
        }
    }

    private void novaVenda() {
        codigo_item.grabFocus();
        itens = new ArrayList<>();
        this.objeto = null;
        this.vender = false;
        this.ajustaComponentes(this.vender);
        montaObejtoTela(this.objeto);
        this.principal.atualizaVendas();
        this.limpar();
    }

}
