/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zizegaitero.ticketbar.eventos.repository;

import com.zizegaitero.ticketbar.connection.EntityManagerUtil;
import com.zizegaitero.ticketbar.entidades.Eventos;
import com.zizegaitero.ticketbar.entidades.Eventos_;
import com.zizegaitero.ticketbar.entidades.WvendasItens;
import com.zizegaitero.ticketbar.entidades.WvendasItens_;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author jean
 */
public class EventoRepository {

    /**
     * lista todos os tipos de produtos
     *
     * @return
     */
    public List<Eventos> findAll() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Eventos> from = query.from(Eventos.class);
        query.select(from);
        List<Eventos> tipoProdutos = em.createQuery(query).getResultList();
        return tipoProdutos;
    }

    /**
     * Busca o Evento ativo para Lançamento
     *
     * @return
     */
    public Eventos findEventoAtivoVenda() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Eventos> from = query.from(Eventos.class);
        query.select(from);
        query.where(cb.between(cb.currentDate(), from.get(Eventos_.dtevento), from.get(Eventos_.dtfimevento)),
                cb.equal(from.get(Eventos_.idativo), Boolean.TRUE));
        query.orderBy(cb.desc(from.get(Eventos_.dtevento)));
        try {
            Eventos evento = (Eventos) em.createQuery(query).
                    setMaxResults(1).
                    getSingleResult();
            return evento;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<WvendasItens> findVendasByEvento(Eventos eve, Date dtini, Date dtfim) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<WvendasItens> from = query.from(WvendasItens.class);
        query.select(from);

        List<Predicate> p = new ArrayList<>();

        if (eve != null) {
            p.add(cb.equal(from.get(WvendasItens_.idevento), BigInteger.valueOf(eve.getId())));
        }

        if (dtini != null && dtfim != null) {
            cb.between(from.get(WvendasItens_.dtevento), dtini, dtfim);
        }

        if (!p.isEmpty()) {
            query.where(p.toArray(new Predicate[]{}));
        }

        query.orderBy(cb.desc(from.get(WvendasItens_.idevento)),
                cb.asc(from.get(WvendasItens_.codigoProduto)));
        List<WvendasItens> evento = em.createQuery(query).getResultList();
        return evento;
    }

    /**
     * salva um tipo de produto
     *
     * @param evento
     * @return
     * @throws Exception
     */
    public Eventos salvaEventos(Eventos evento) throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        em.getTransaction().begin();
        evento.setDthrcadastro(new Date());
        em.merge(evento);
        em.getTransaction().commit();
        return evento;
    }

    /**
     * Busca o próximo código
     *
     * @return
     * @throws Exception
     */
    public Long buscaProximoSequencial() throws Exception {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Eventos> from = query.from(Eventos.class);
        query.select(from);
        query.orderBy(cb.desc(from.get(Eventos_.codigo)));
        try {
            Eventos tipoProduto = (Eventos) em.createQuery(query).
                    setMaxResults(1).
                    getSingleResult();
            return tipoProduto.getCodigo() + 1;
        } catch (NoResultException ex) {
            return 1L;
        }
    }

    /**
     * busca o logo do evento, se não tiver pega o logo da empresa
     *
     * @param idEvento
     * @return
     */
    public byte[] findLogoEvento(Long idEvento) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Eventos eve = em.find(Eventos.class, idEvento);
        if (eve.getLogotipo() == null) {
            return eve.getIdEmpresa().getLogotipo();
        }
        return eve.getLogotipo();
    }
    
    public String findNomeEmpresa(Long idEvento) {
        EntityManager em = EntityManagerUtil.getEntityManager();
        Eventos eve = em.find(Eventos.class, idEvento);
        return eve.getIdEmpresa().getNome();
    }

    public List<Eventos> findAllAtivos() {
        EntityManager em = EntityManagerUtil.getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery();
        Root<Eventos> from = query.from(Eventos.class);
        query.select(from);
        query.where(cb.equal(from.get(Eventos_.idativo), Boolean.TRUE));
        List<Eventos> tipoProdutos = em.createQuery(query).getResultList();
        return tipoProdutos;
    }

}
