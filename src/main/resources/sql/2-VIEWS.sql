
CREATE VIEW PUBLIC.WPRODUTOS_COMBOS AS 

	SELECT PROD.CODIGO, INFO.ID AS IDPRODUTOINFO, NULL AS IDCOMBOINFO, PROD.DESCRICAO, INFO.VALOR, PROD.IDATIVO, 'PRODUTOS' AS TIPO 
	FROM PUBLIC.PRODUTOS_INFO INFO
		JOIN PRODUTOS PROD ON PROD.ID = INFO.ID_PRODUTO
	WHERE INFO.IDATIVO = TRUE
	UNION ALL
	SELECT PROD.CODIGO, NULL AS IDPRODUTOINFO, INFO.ID AS IDCOMBOINFO, PROD.DESCRICAO, INFO.VALOR, PROD.IDATIVO, 'COMBO' AS TIPO 
	FROM PUBLIC.COMBO_INFO INFO
		JOIN COMBO PROD ON PROD.ID = INFO.ID_COMBO
	WHERE INFO.IDATIVO = TRUE	


CREATE VIEW WVENDAS_POR_PRODUTO AS 
SELECT ITEM.ID||'combo'||ite.ID AS id, VEND.ID AS IDVENDA, PROD.CODIGO, PROD.DESCRICAO, 
	ITEM.QUANTIDADE*ITE.QUANTIDADE AS QUANTIDADE, 
	(ITE.VALOR*1.00000/ITE.QUANTIDADE) AS VALOR_UNIT, 
	ITEM.QUANTIDADE*ITE.VALOR AS VALOR_TOTAL, 
	COMBO.DESCRICAO DESCCOMBO,
	EVE.ID AS IDEVENTO,
	EVE.DESCRICAO AS DESCRICAO_EVENTO,
	EVE.DTEVENTO,
	VEND.DTHRCADASTRO,
	item.id AS idvenda_item
FROM PUBLIC.VENDAS_ITEM ITEM
	JOIN PUBLIC.VENDAS VEND ON VEND.ID = ITEM.ID_VENDA
	JOIN PUBLIC.COMBO_INFO INFO ON INFO.ID = ITEM.ID_COMBO_INFO
	JOIN PUBLIC.COMBO_ITEM ITE ON ITE.ID_COMBO_INFO = INFO.ID
	JOIN PUBLIC.COMBO COMBO ON COMBO.ID = INFO.ID_COMBO
	JOIN PUBLIC.PRODUTOS_INFO PRODIN ON PRODIN.ID = ITE.ID_PRODUTO_INFO
	JOIN PUBLIC.PRODUTOS PROD ON PROD.ID = PRODIN.ID_PRODUTO
	JOIN PUBLIC.EVENTOS EVE ON EVE.ID = VEND.ID_EVENTO
WHERE VEND.IDCANCELADO = FALSE	
UNION ALL
SELECT ITEM.ID||'produto' AS id, VEND.ID AS IDVENDA, PROD.CODIGO, PROD.DESCRICAO, 
	ITEM.QUANTIDADE AS QUANTIDADE, 
	ITEM.VALOR AS VALOR_UNIT, 
	ITEM.QUANTIDADE*ITEM.VALOR AS VALOR_TOTAL,
	NULL DESCCOMBO,
	EVE.ID AS IDEVENTO,
	EVE.DESCRICAO AS DESCRICAO_EVENTO,
	EVE.DTEVENTO,
	VEND.DTHRCADASTRO,
	item.id AS idvenda_item
FROM PUBLIC.VENDAS_ITEM ITEM
	JOIN PUBLIC.VENDAS VEND ON VEND.ID = ITEM.ID_VENDA
	JOIN PUBLIC.PRODUTOS_INFO PRODIN ON PRODIN.ID = ITEM.ID_PRODUTO_INFO
	JOIN PUBLIC.PRODUTOS PROD ON PROD.ID = PRODIN.ID_PRODUTO
	JOIN PUBLIC.EVENTOS EVE ON EVE.ID = VEND.ID_EVENTO
WHERE VEND.IDCANCELADO = FALSE

CREATE VIEW WVENDAS_ITENS AS 
SELECT ITEM.ID, VEND.ID AS IDVENDA, COMBO.CODIGO AS CODIGO_PRODUTO, 
	COMBO.DESCRICAO AS DESCRICAO_PRODUTO, 
	ITEM.QUANTIDADE AS QUANTIDADE,  
	ITEM.VALOR AS VALOR,
	ITEM.QUANTIDADE*ITEM.VALOR AS VALOR_TOTAL,
	VEND.CODIGO AS CODIGO_VENDA,
	VEND.DTHRCADASTRO,
	VEND.VALOR_VENDA,
	EVE.DESCRICAO,
	EVE.DTEVENTO,
	EVE.ID AS IDEVENTO
FROM PUBLIC.VENDAS_ITEM ITEM
	JOIN PUBLIC.VENDAS VEND ON VEND.ID = ITEM.ID_VENDA
	JOIN PUBLIC.COMBO_INFO INFO ON INFO.ID = ITEM.ID_COMBO_INFO
	JOIN PUBLIC.COMBO COMBO ON COMBO.ID = INFO.ID_COMBO
	JOIN PUBLIC.EVENTOS EVE ON EVE.ID = VEND.ID_EVENTO
WHERE VEND.IDCANCELADO = FALSE	
UNION ALL
SELECT ITEM.ID, VEND.ID AS IDVENDA, 
	PROD.CODIGO AS CODIGO_PRODUTO, 
	PROD.DESCRICAO AS DESCRICAO_PRODUTO, 
	ITEM.QUANTIDADE AS QUANTIDADE,  
	ITEM.VALOR AS VALOR,
	ITEM.QUANTIDADE*ITEM.VALOR AS VALOR_TOTAL,
	VEND.CODIGO AS CODIGO_VENDA,
	VEND.DTHRCADASTRO,
	VEND.VALOR_VENDA,
	EVE.DESCRICAO,
	EVE.DTEVENTO,
	EVE.ID AS IDEVENTO
FROM PUBLIC.VENDAS_ITEM ITEM
	JOIN PUBLIC.VENDAS VEND ON VEND.ID = ITEM.ID_VENDA
	JOIN PUBLIC.PRODUTOS_INFO INFO ON INFO.ID = ITEM.ID_PRODUTO_INFO
	JOIN PUBLIC.PRODUTOS PROD ON PROD.ID = INFO.ID_PRODUTO
	JOIN PUBLIC.EVENTOS EVE ON EVE.ID = VEND.ID_EVENTO
WHERE VEND.IDCANCELADO = FALSE	